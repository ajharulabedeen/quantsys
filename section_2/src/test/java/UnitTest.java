import org.junit.jupiter.api.Test;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.quantsys.PrimeNumberTest;


public class UnitTest {

    private static final Logger logger = LogManager.getLogger(UnitTest.class);
    {
        BasicConfigurator.configure();
    }
    @Test
    public void primeTest() {
        logger.info("Prime Test");
        assertEquals(PrimeNumberTest.isPrime(0), false);
        assertEquals(PrimeNumberTest.isPrime(1), false);
        assertEquals(PrimeNumberTest.isPrime(2), true);//prime
        assertEquals(PrimeNumberTest.isPrime(3), true);//prime
        assertEquals(PrimeNumberTest.isPrime(4), false);
        assertEquals(PrimeNumberTest.isPrime(13), true);//prime
    }
}
