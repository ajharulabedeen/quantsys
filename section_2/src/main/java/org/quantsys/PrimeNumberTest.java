package org.quantsys;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
public class PrimeNumberTest {

    private static final Logger logger = LogManager.getLogger(PrimeNumberTest.class);

    public static boolean isPrime(int n){
        int i, mid=0,flag=0;
        mid = n/2;

        if(n==0||n==1){
            logger.info(n+" is not prime number");
            return false;
        }else{
            for(i=2;i<=mid;i++){
                if(n%i==0){
                    logger.info(n+" is not prime number");
                    flag=1;
                    break;
                }
            }
            if(flag==0)  {
                logger.info(n+" is prime number");
                return true;
            }else {
                return false;
            }
        }//end of else
    }
}
